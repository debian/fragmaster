# Translation of fragmaster man page template to German
# Copyright (C) Helge Kreutzmann <debian@helgefjell.de>, 2011, 2014.
# This file is distributed under the same license as the fragmaster package.
#
msgid ""
msgstr ""
"Project-Id-Version: debianutils man page\n"
"POT-Creation-Date: 2014-01-14 15:31+0100\n"
"PO-Revision-Date: 2014-01-16 20:29+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: de <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: utf-8\n"

#. type: =head1
#: po4a/fragmaster:455
msgid "NAME"
msgstr "NAME"

#. type: textblock
#: po4a/fragmaster:457
msgid "fragmaster - Using psfrag constructs with pdflatex"
msgstr "fragmaster - Psfrag-Konstrukte mit Pdflatex verwenden"

#. type: =head1
#: po4a/fragmaster:459
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: verbatim
#: po4a/fragmaster:461
#, no-wrap
msgid ""
" fragmaster [options]\n"
"\n"
msgstr ""
" fragmaster [OPTIONEN]\n"
"\n"

#. type: verbatim
#: po4a/fragmaster:463
#, no-wrap
msgid ""
" Create EPS and PDF files with embedded psfrag substitutions.\n"
"\n"
msgstr ""
"EPS- und PDF-Dateien mit eingebetteten Psfrag-Ersetzungen erstellen.\n"
"\n"

#. type: verbatim
#: po4a/fragmaster:465
#, no-wrap
msgid ""
" Options:\n"
"  -h,--help          Brief usage guide.\n"
"  -m,--man           Show full man page (needs perldoc)\n"
"  --clean            Clean auto-generated $basename.{eps,pdf} files.\n"
"  --debug            Show more info and leave temporary files behind.\n"
"  --force            Rebuild everything ignoring modification times.\n"
"  --dirfm=file       Use given file as optional per-directory\n"
"                     fragmaster fm control file instead of default\n"
"                     \"fragmaster.dfm\".\n"
"\n"
msgstr ""
" Optionen:\n"
"  -h,--help          kurze Benutzungsanleitung\n"
"  -m,--man           komplette Handbuchseite zeigen (benötigt Perldoc)\n"
"  --clean            automatisch erzeugte $basename.{eps,pdf}-Dateien bereinigen\n"
"  --debug            weitere Infos zeigen und temporäre Dateien hinterlassen\n"
"  --force            alles neu bauen, Änderungszeiten ignorieren.\n"
"  --dirfm=Datei      angegebene Datei als optionale, verzeichnisweite\n"
"                     fragmaster-fm-Steuerdatei statt der Vorgabe\n"
"                     »fragmaster.dfm« verwenden.\n"
"\n"

#. type: =head1
#: po4a/fragmaster:475
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: textblock
#: po4a/fragmaster:477
msgid ""
"B<fragmaster> is a perl script that helps using psfrag constructs with "
"B<pdflatex>."
msgstr ""
"B<fragmaster> ist ein Perl-Skript, das beim Einsatz von Psfrag-Konstrukten "
"mit B<pdflatex> hilft."

#. type: textblock
#: po4a/fragmaster:480
msgid ""
"B<psfrag> is a LaTeX package which allows to replace text elements in "
"included EPS graphics by arbitrary LaTeX output. Because B<psfrag> uses "
"PostScript for making the replacements, in principle you can't use B<psfrag> "
"with B<pdflatex> which doesn't have any interfaces to PostScript."
msgstr ""
"B<psfrag> ist ein LaTeX-Paket, das die Ersetzung von Textelementen in "
"eingefügten EPS-Graphiken durch beliebige LaTeX-Ausgabe erlaubt. Da "
"B<psfrag> PostScript für die Ersetzung verwendet, können Sie prinzipiell "
"B<psfrag> nicht mit B<pdflatex> verwenden, da dieses keine Schnittstelle zu "
"PostScript hat."

#. type: textblock
#: po4a/fragmaster:486
msgid ""
"B<fragmaster> produces a new EPS from your original EPS which already "
"contains all those B<psfrag> replacements. This new EPS graphic actually can "
"be converted to PDF including all replacements. The resulting \"encapsulated"
"\" PDF can then be used with pdflatex."
msgstr ""
"B<fragmaster> erstellt eine neue EPS aus ihrer ursprünglichen EPS, welche "
"bereits alle diese B<psfrag>-Ersetzungen enthält. Diese neue EPS-Graphik "
"kann dann in der Tat mit allen Ersetzungen in PDF gewandelt werden. Das so "
"erstellte »eingekapselte« PDF kann dann mit Pdflatex verwandt werden."

#. type: textblock
#: po4a/fragmaster:491
msgid ""
"B<fragmaster> will scan the current directory for files which end in F<_fm> "
"and have a F<_fm.eps> counterpart.  Looking at the modification dates, the "
"script checks if the output files have to be rebuilt and does so if "
"necessary (a little like \"make\" would do it)."
msgstr ""
"B<fragmaster> durchsucht das aktuelle Verzeichnis nach Dateien, die in "
"F<_fm> enden und ein F<_fm.eps> Gegenstück haben. Durch Prüfen des "
"Veränderungszeitpunkts ermittelt das Skript, ob die Ausgabedateien neu "
"erstellt werden müssen und erledigt dies, falls notwendig (ein bisschen, wie "
"dies auch »make« durchführen würde)."

#. type: textblock
#: po4a/fragmaster:497
msgid "In your LaTeX document you can include the produced graphics using"
msgstr ""
"Mittels folgendem Befehl fügen Sie die erstellte Graphik in Ihr LateX-"
"Dokument ein:"

#. type: verbatim
#: po4a/fragmaster:499
#, no-wrap
msgid ""
" \\includegraphics{<graphics>}\n"
"\n"
msgstr ""
" \\includegraphics{<grafik>}\n"
"\n"

#. type: textblock
#: po4a/fragmaster:501
msgid ""
"conveniently omitting file extension.  B<latex> will choose the EPS, "
"B<pdflatex> will choose the PDF."
msgstr ""
"Hierbei können Sie bequem die Endung entfallen lassen. B<latex> wird die EPS-"
"Variante, B<pdflatex> die PDF-Variante auswählen."

#. type: =head2
#: po4a/fragmaster:504
msgid "B<fragmaster> control file and other related files."
msgstr "B<fragmaster>s Steuerdatei und andere verwandte Dateien"

#. type: textblock
#: po4a/fragmaster:506
msgid "To use the script you have to create two files per graphic:"
msgstr ""
"Um das Skript zu verwenden, müssen Sie zwei Dateien pro Graphik erstellen:"

#. type: verbatim
#: po4a/fragmaster:508
#, no-wrap
msgid ""
"    * <graphics>_fm.eps: the EPS file itself,\n"
"    * <graphics>_fm: a fragmaster control file.\n"
"\n"
msgstr ""
"    * <graphik>_fm.eps: die EPS-Datei selbst\n"
"    * <graphik>_fm: eine Fragmaster-Steuerdatei\n"
"\n"

#. type: textblock
#: po4a/fragmaster:511
msgid "From these files the psfragged graphics will be created:"
msgstr "Aus diesen Dateien wird die Psfraggte-Graphik erstellt:"

#. type: verbatim
#: po4a/fragmaster:513
#, no-wrap
msgid ""
"    * <graphics>.eps,\n"
"    * <graphics>.pdf\n"
"\n"
msgstr ""
"    * <graphik>.eps,\n"
"    * <graphik>.pdf\n"
"\n"

#. type: textblock
#: po4a/fragmaster:516
msgid ""
"The F<_fm> control file is basically a LaTeX file (with optionally special "
"comments) and can look like this:"
msgstr ""
"Die Steuerdatei von F<_fm> ist im Prinzip eine LaTeX-Datei (mit optionalen "
"besonderen Kommentaren). Sie kann wie folgt ausschauen:"

#. type: verbatim
#: po4a/fragmaster:519
#, no-wrap
msgid ""
" % Just an ordinary comment\n"
" %\n"
" % Some special comments:\n"
" % fmclass: book\n"
" % fmclassopt: 11pt\n"
" % fmopt: width=6cm\n"
" %\n"
" % Another special comment:\n"
" % head:\n"
" % \\usepackage{amsmath}\n"
" % end head\n"
"\n"
msgstr ""
" % Nur ein normaler Kommentar\n"
" %\n"
" % Einige besondere Kommentare:\n"
" % fmclass: book\n"
" % fmclassopt: 11pt\n"
" % fmopt: width=6cm\n"
" %\n"
" % Ein weiterer besonderer Kommentar:\n"
" % head:\n"
" % \\usepackage{amsmath}\n"
" % end head\n"
"\n"

#. type: verbatim
#: po4a/fragmaster:531
#, no-wrap
msgid ""
" % psfrag commands:\n"
" \\psfrag{x}{$x$}\n"
" \\psfrag{y}{$y = x^2$}\n"
"\n"
msgstr ""
" % psfrag-Befehle:\n"
" \\psfrag{x}{$x$}\n"
" \\psfrag{y}{$y = x^2$}\n"
"\n"

#. type: textblock
#: po4a/fragmaster:535
msgid ""
"Special comment C<fmclass:> will make the script use given class instead of "
"default C<article> class."
msgstr ""
"Der besondere Kommentar C<fmclass:> führt dazu, dass das Skript die "
"angegebene Klasse statt der Standardklasse C<article> benutzt."

#. type: textblock
#: po4a/fragmaster:538
msgid ""
"Special comment C<fmclassopt:> will make the script use given options as "
"class options instead of default C<12pt>."
msgstr ""
"Der besondere Kommentar C<fmclassopt:> führt dazu, dass das Skript die "
"angegebene Klassenoptionen statt des standardmäßigen C<12pt> benutzt."

#. type: textblock
#: po4a/fragmaster:541
msgid ""
"The special comment C<fmopt:> will be evaluated such that the following text "
"will by passed as optional argument to C<\\includegraphics>.  This way you "
"can e.g. adjust the relation between graphics size and font size using "
"something like C<fmopt: width=6cm>.  No global default for this."
msgstr ""
"Der besondere Kommentar C<fmopt:> wird so evaluiert, dass der folgende Text "
"als optionales Argument an C<\\includegraphics> weitergegeben wird. Damit "
"können Sie beispielsweise den Bezug zwischen Graphik- und Schriftgröße mit "
"Kommentaren der Art C<fmopt: width=6cm> anpassen. Hierfür gibt es keine "
"globale Voreinstellung."

#. type: textblock
#: po4a/fragmaster:548
msgid ""
"The special comment construct C<head:/end head> causes the lines in between "
"to be included in the preamble of the LaTeX temporary document after having "
"the leading comment characters \"%\" stripped off.  This way, you can "
"include LaTeX packages, as in C<\\usepackage{amsmath}>.  No global default "
"for this."
msgstr ""
"Das besondere Kommentarkonstrukt C<head:/end head> führt dazu, dass die "
"Zeilen dazwischen in der Präambel des temporären LaTeX-Dokuments eingefügt "
"werden, wobei die führenden Kommentarzeichen »%« entfernt werden. Auf diese "
"Weise können Sie LaTeX-Pakete einbinden, beispielsweise C<"
"\\usepackage{amsmath}>. Hierfür gibt es kein globalen Vorgabewert."

#. type: =head2
#: po4a/fragmaster:554
msgid "Per-directory B<fragmaster> dir control file."
msgstr "Verzeichnisweite B<fragmaster>-Verzeichnissteuerdatei."

#. type: textblock
#: po4a/fragmaster:556
msgid ""
"You can set per-directory C<fmclass:>, C<fmclassopt:>, C<fmopt:> and C<head:/"
"end head> options by means of a per-directory fragmaster control file "
"F<fragmaster.dfm> with similar syntax as above.  You can use another file by "
"means of the B<--dirfm> option.  Note that options set this way are mutually "
"exclusive, any option set in per-file F<_fm> file will completely override "
"associated option in per-directory file, and options set in per-directory "
"file will override initial defaults (C<\\documentclass[12pt]{article}>).  "
"Empty options are ignored."
msgstr ""
"Sie können verzeichnisweite Optionen C<fmclass:>, C<fmclassopt:>, C<fmopt:> "
"und C<head:/end head> durch eine verzeichnisweite Fragmaster-Steuerdatei "
"F<fragmaster.dfm> mit ähnlicher Syntax wie oben setzen. Sie können eine "
"andere mit der Option B<--dirfm> verwenden. Beachten Sie, dass so gesetzte "
"Optionen paarweise ausschließend sind. Jede dateiabhängige F<_fm>-Datei wird "
"die zugeordneten Optionen in der verzeichnisweiten Datei komplett "
"überschreiben und Optionen in verzeichnisweiten Dateien werden die "
"anfänglichen Vorgaben (C<\\documentclass[12pt]{article}>) überschreiben. "
"Leere Optionen werden ignoriert."

#. type: textblock
#: po4a/fragmaster:566
msgid ""
"This is work in progress and still needs extensive checking. Double-check "
"that modification date based rebuilds are working properly."
msgstr ""
"Diese Funktionalität ist noch in Arbeit und benötigt umfassende Prüfungen. "
"Überprüfen Sie zweimal, dass änderungsbasierte Neuerstellungen korrekt "
"funktionieren."

#. type: =head1
#: po4a/fragmaster:569
msgid "KNOWN PROBLEMS"
msgstr "BEKANNTE PROBLEME"

#. type: textblock
#: po4a/fragmaster:571
msgid ""
"In case the EPS will be produced as landscape graphics, i.e. B<gv> shows "
"I<Landscape> instead of I<Portrait> in the menu bar, and the graphic will "
"end up turned around 90 degrees in your document, then it is likely that "
"your original EPS is wider than it is tall.  In this case some (more recent) "
"versions of B<dvips> make the \"smart\" assumption that your graphic is "
"landscape, even though the graphic's proportions don't tell anything about "
"the orientation of its contents...  This still can happen in case your input "
"EPS matches a standard paper size."
msgstr ""
"Falls das EPS im Querformat erstellt wird, d.h. B<gv> zeigt I<Querformat> "
"statt I<Hochformat> in der Menüzeile an und die Graphik um 90° gedreht in "
"Ihrem Dokument landet, dann ist es wahrscheinlich, dass Ihre ursprüngliche "
"EPS breiter als hoch ist. In diesem Fall vermuten einige (neuere) Versionen "
"von B<dvips> »intelligent«, dass Ihre Graphik im Querformat vorliegt, obwohl "
"die Proportionen der Graphik nichts über die Ausrichtung des Inhalts "
"aussagen ... Das kann sogar passieren, wenn Ihre Eingabe-EPS eine Standard-"
"Papiergröße hat."

#. type: textblock
#: po4a/fragmaster:580
msgid ""
"Anyway, you can make B<dvips> behave nicer by specifying the following line "
"in F</usr/share/texmf/dvips/config/config.pdf> (or a local equivalent inside "
"F</usr/local/share/texmf>):"
msgstr ""
"Sie können auf jeden Fall ein netteres Verhalten von B<dvips> erreichen, "
"indem Sie die folgende Zeile in F</usr/share/texmf/dvips/config/config.pdf> "
"(oder dem lokalen Äquivalent F</usr/local/share/texmf>) angeben:"

#. type: textblock
#: po4a/fragmaster:584
msgid "@ custom 0pt 0pt"
msgstr "@ custom 0pt 0pt"

#. type: textblock
#: po4a/fragmaster:586
msgid ""
"In the likely case that you're wondering why, I'd recommend the B<dvipsk> "
"sources warmly to you..."
msgstr ""
"In dem wahrscheinlichen Fall, dass Sie sich nach dem Warum fragen, empfehle "
"ich Ihnen wärmstens die Quellen von B<dvipsk> ..."

#. type: =head1
#: po4a/fragmaster:589
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: textblock
#: po4a/fragmaster:591
msgid ""
"Tilman Vogel <tilman vogel web de> (dot at dot) and Agustin Martin <agustin "
"martin hispalinux es> (dot at dot)"
msgstr ""
"Tilman Vogel <tilman vogel web de> (Punkt at Punkt) und Agustin Martin "
"<agustin martin hispalinux es> (Punkt at Punkt)"

#. type: =head1
#: po4a/fragmaster:594
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: textblock
#: po4a/fragmaster:596
msgid ""
"This script was inspired by a posting from Karsten Roemke <k roemke gmx de> "
"(dot at dot) with subject \"psfrag pdflatex, lange her\" in de.comp.text.tex "
"on 2003-11-11 05:25:44 PST."
msgstr ""
"Dieses Skript wurde von einer Nachricht von Karsten Roemke <k roemke gmx de> "
"(Punkt at Punkt) am 2003-11-11 um 05:25:44 PST in de.comp.text.tex mit dem "
"Betreff »psfrag pdflatex, lange her« inspiriert."

#. type: textblock
#: po4a/fragmaster:601
msgid ""
"Karsten Roemke was inspired for his solution by postings from Thomas Wimmer."
msgstr ""
"Karsten Roemke wurde durch Nachrichten von Thomas Wimmer zu seiner Lösung "
"inspiriert."

#. type: =head1
#: po4a/fragmaster:604
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: verbatim
#: po4a/fragmaster:606
#, no-wrap
msgid ""
" Copyright (C) 2004 Tilman Vogel\n"
" Copyright (C) 2011-2014 Agustin Martin\n"
"\n"
msgstr ""
" Copyright (C) 2004 Tilman Vogel\n"
" Copyright (C) 2011-2014 Agustin Martin\n"
"\n"

#. type: textblock
#: po4a/fragmaster:609
msgid ""
"This program is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation; either version 2 of the License, or (at your option) "
"any later version."
msgstr ""
"Dieses Programm ist freie Software; Sie können es unter den Bedingungen der "
"GNU General Public License, entweder Version 2 der Lizenz oder (wenn Sie es "
"wünschen) jeder neueren Version, wie diese von der Free Software Foundation "
"veröffentlicht wurden, vertreibend und/oder ändern."
